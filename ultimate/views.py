# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt


gameData = {
    'id': 1,
    'board': [
        ['','','','','','','','',''],
        ['','','','','','','','',''],
        ['','','','','','','','',''],
        ['','','','','','','','',''],
        ['','','','','','','','',''],
        ['','','','','','','','',''],
        ['','','','','','','','',''],
        ['','','','','','','','',''],
        ['','','','','','','','',''],
    ],
    'winner': '',
    'turn': 'X',
    'valid_subgames': [0,1,2,3,4,5,6,7,8]
}

@csrf_exempt
def game(request):
    if request.method == 'POST':
        return JsonResponse(gameData, safe=False)

    if request.method == 'GET':
        if request.GET.get('id') == '1':
            return JsonResponse(gameData, safe=False)
        else:
            return JsonResponse({
                'Error': 'Invalid id'
            })

@csrf_exempt
def move(request):
    subgame = int(request.POST.get('subgame'))
    cell = int(request.POST.get('cell'))

    if subgame in gameData['valid_subgames']:

        if gameData['board'][subgame][cell] == u'':
            gameData['board'][subgame][cell] = gameData['turn']
            gameData['turn'] = 'O' if gameData['turn'] == 'X' else 'X'
            gameData['valid_subgames'] = [cell]
            gameData['winner'] = getWinner(gameData['board'])

            return JsonResponse(gameData, safe=False)
        else:
            return JsonResponse({
                'Error': 'Cell occupied'
            })
    else:
        return JsonResponse({
            'Error': 'Invalid move: subgame'
        })

def checkRows(subgame, player):
    for row in [0,1,2]:
        cells = 0
        for cell in [0,1,2]:
            if subgame[row+cell] == player:
                cells += 1
            if cells == 3:
                return True

def checkColumns(subgame, player):
    for col in [0,1,2]:
        cells = 0
        for cell in [0,3,6]:
            if subgame[col+cell] == player:
                cells += 1
            if cells == 3:
                return True

def checkDiagonals(subgame, player):
    for diagonal in [[0,4,8], [2,4,6]]:
        cells = 0
        for cell in diagonal:
            if subgame[cell] == player:
                cells += 1
            if cells == 3:
                return True

def checkSubgame(subgame, player):
    if checkRows(subgame, player) or checkColumns(subgame, player) or checkDiagonals(subgame, player):
        return True

def getWinner(board):
    for player in ['X', 'O']:
        for subgame in board:
            if checkSubgame(subgame, player):
                return player

    return ''
